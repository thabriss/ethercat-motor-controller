EtherCAT Motor Controller
=========================

Wprowadzenie
------------
Aplikacja EtherCAT Motor Controller służy do sterowania napędem opartym o moduł EL7041 z komputera działającego pod kontrolą systemu Linux.

Wymagania
---------
- system Linux (testowane na Ubuntu 18.04)
- Qt 4.8
- biblioteka pthread