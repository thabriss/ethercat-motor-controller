#ifndef APITYPES_H
#define APITYPES_H
#include <inttypes.h>
typedef union {
    uint8_t uint8Value[4];
    int8_t int8Value[4];
    uint16_t uint16Value[2];
    int16_t int16Value[2];
    uint32_t uint32Value;
    int32_t int32Value;
  } VAR;
#endif // APITYPES_H
