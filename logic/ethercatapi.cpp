﻿#include "ethercatapi.h"
#include <sched.h>

typedef struct
{
    const char * interface;
    char * ioMap;
}COM_THREAD_STRUCT_T;
static COM_THREAD_STRUCT_T comTreadGlobal;
static int expectedWKC;
static bool needlf;
static volatile int wkc;
static bool inOP;
static uint8_t currentgroup = 0;
static char usdo[128];
static int lenght = sizeof(usdo) - 1;
static bool stopThred =false;
static void * ecatCheck( void *ptr );
static pthread_t ecCheck;
static void *pdoComunication(void* arg);
static pthread_t comunication;
static struct sched_param pdoCommParam;
static pthread_attr_t pdoCommAttr;
static int ret = 0;

using namespace EtherCatApi;
uint32 network_configuration(void)
{
   // Do we got expected number of slaves from config/
   if (ec_slavecount < NUMBER_OF_SLAVES)
{	return 0;
}
   // Verify slave by slave that it is correct
   if (strcmp(ec_slave[EK1100_1].name,"EK1100")){
     return 0;
}
   else if (strcmp(ec_slave[EL7041_1].name,"EL7041")){
     return 0;
}
  return 1;
}

void * pdoComunication(void * arg)
{
    int i, chk;
    needlf = FALSE;
    inOP = FALSE;

   COM_THREAD_STRUCT_T comThreadLocal = *(reinterpret_cast<COM_THREAD_STRUCT_T *>(arg));
   if (ec_init(comThreadLocal.interface))
   {
      printf("Skonfigurowano prawidlowo wyjscie %s\n", comThreadLocal.interface);

      if( ec_config_init(FALSE) >0)
      {
         if(network_configuration())
         {
            printf("Urzadzenia zostaly prawidlowo zestawione\n");
            ec_config_map(reinterpret_cast<void*>(comThreadLocal.ioMap));
            ec_configdc();
            printf("Mapowanie pamieci urzadzen oraz oczekiwanie na przejscie w stan SAFE_OPERATIONAL.\n");
            ec_statecheck(0, EC_STATE_SAFE_OP,  EC_TIMEOUTSTATE * 4);
            printf("Proba przejscia w stan operational\n");
            expectedWKC = (ec_group[0].outputsWKC * 2) + ec_group[0].inputsWKC;
            printf("Obliczanie ilosci urzadzen %d\n", expectedWKC);
            ec_slave[0].state = EC_STATE_OPERATIONAL;
            ec_send_processdata();
            ec_receive_processdata(EC_TIMEOUTRET);
            ec_writestate(0);
            chk = 40;
            do
            {
                ec_send_processdata();
                ec_receive_processdata(EC_TIMEOUTRET);
                ec_statecheck(0, EC_STATE_OPERATIONAL, 50000);
                i++;
            }while (chk-- && (ec_slave[0].state != EC_STATE_OPERATIONAL));
            if (ec_slave[0].state == EC_STATE_OPERATIONAL )
            {
                printf("Stan OPERATIONAL otrzymany dla wszyskich urznen.\n");
                inOP = TRUE;
                /* cyclic loop */
                while(!stopThred)
                {
                    ec_send_processdata();
                    wkc = ec_receive_processdata(EC_TIMEOUTRET);
                    osal_usleep(100);

                }
            }
            else
            {
                printf("Nie wszystkie urzadzenia weszly w stan OPERATIONAL.\n");
                ec_readstate();
                for(i = 1; i<=ec_slavecount ; i++)
                {
                    if(ec_slave[i].state != EC_STATE_OPERATIONAL)
                    {
                        printf("Urzadzenie %d Stan=0x%2.2x StatusCode=0x%4.4x : %s\n",
                            i, ec_slave[i].state, ec_slave[i].ALstatuscode, ec_ALstatuscode2string(ec_slave[i].ALstatuscode));
                    }
                }
            }
            printf("\nWyslanie do wszystkich urzaden stanu INIT\n");
            ec_slave[0].state = EC_STATE_INIT;
            ec_writestate(0);
          }
          else
          {
            printf("Nie zestawiono prawidowo urzadzen");
          }
        }
        else
        {
            printf("Nie znaleziono okresslonych urzadzen!\n");
        }
        ec_close();
    }
    else
    {
        printf("Nie udane przygotowanie urz¹dzenia %s\n", comThreadLocal.interface);
    }
    ret = 2;
    inOP = FALSE;
    pthread_exit(&ret);
}

void * ecatCheck( void *arg )
{
    int slave;
    (void)arg;                  /* Not used */

    while(!stopThred)
    {
        if( inOP && ((wkc < expectedWKC) || ec_group[currentgroup].docheckstate))
        {
            if (needlf)
            {
               needlf = FALSE;
               printf("\n");
            }
            /* one ore more slaves are not responding */
            ec_group[currentgroup].docheckstate = FALSE;
            ec_readstate();
            for (slave = 1; slave <= ec_slavecount; slave++)
            {
               if ((ec_slave[slave].group == currentgroup) && (ec_slave[slave].state != EC_STATE_OPERATIONAL))
               {
                  ec_group[currentgroup].docheckstate = TRUE;
                  if (ec_slave[slave].state == (EC_STATE_SAFE_OP + EC_STATE_ERROR))
                  {
                     printf("Blad: slave %d jest w stanie SAFE_OP + ERROR, proba potwierdzenia.\n", slave);
                     printf("Blad: %s", ec_elist2string());
                     ec_slave[slave].state = (EC_STATE_SAFE_OP + EC_STATE_ACK);
                     ec_writestate(slave);
                  }
                  else if(ec_slave[slave].state == EC_STATE_SAFE_OP)
                  {
                     printf("Ostrzezenie : slave %d jest w stanie SAFE_OP, proba zmiany na OPERATIONAL.\n", slave);
                     ec_slave[slave].state = EC_STATE_OPERATIONAL;
                     ec_writestate(slave);
                  }
                  else if(ec_slave[slave].state > EC_STATE_NONE)
                  {
                     if (ec_reconfig_slave(slave, EC_TIMEOUTMON))
                     {
                        ec_slave[slave].islost = FALSE;
                        printf("WIADOMOSC : slave %d ponownie skonfigurowano\n",slave);
                     }
                  }
                  else if(!ec_slave[slave].islost)
                  {
                     ec_statecheck(slave, EC_STATE_OPERATIONAL, EC_TIMEOUTRET);
                     if (ec_slave[slave].state == EC_STATE_NONE)
                     {
                        ec_slave[slave].islost = TRUE;
                        printf("BLAD : slave %d przestal odpowiadac\n",slave);
                     }
                  }
               }
               if (ec_slave[slave].islost)
               {
                  if(ec_slave[slave].state == EC_STATE_NONE)
                  {
                     if (ec_recover_slave(slave, EC_TIMEOUTMON))
                     {
                        ec_slave[slave].islost = FALSE;
                        printf("WIADOMOSC : odzykano komunikacje zurzadzeniem slave %d \n",slave);
                     }
                  }
                  else
                  {
                     ec_slave[slave].islost = FALSE;
                     printf("MESSAGE : znaleziono slave %d \n",slave);
                  }
               }
            }
            if(!ec_group[currentgroup].docheckstate)
               printf("OK : wszystkie urzadzenia w stanie OPERATIONAL.\n");
        }
        osal_usleep(10000);
    }
    ret = 1;
    pthread_exit(&ret);
}

void EtherCatApi::startEtherCat(void * interface, void * ioMapPtr)
{
    comTreadGlobal.ioMap = (char *) ioMapPtr;
    comTreadGlobal.interface = (char *) interface;
    cpu_set_t cpuset;
    pthread_create(&ecCheck, NULL, &ecatCheck, NULL);

    pthread_attr_init(&pdoCommAttr);
    pthread_attr_setschedpolicy(&pdoCommAttr, SCHED_FIFO);
    pdoCommParam.__sched_priority = 80;
    pthread_attr_setschedparam(&pdoCommAttr, &pdoCommParam);
    pthread_create(&comunication, &pdoCommAttr, &pdoComunication, reinterpret_cast<void*>(&comTreadGlobal));
    CPU_SET(3, &cpuset);
    pthread_setaffinity_np(comunication, sizeof(cpuset), &cpuset);
    stopThred = false;
}

void EtherCatApi::stopEtherCat()
{
    stopThred = true;
}

bool EtherCatApi::getInOp()
{
    return inOP;
}

bool EtherCatApi::sdoGetBit (uint16_t slave, uint16_t index, uint8_t subidx)
{
    lenght = sizeof(usdo) - 1;
    memset(&usdo, 0, 128);
    ec_SDOread(slave, index, subidx, FALSE, &lenght, &usdo, EC_TIMEOUTRXM);
    return static_cast<bool>(usdo[0]);
}
uint8_t EtherCatApi::sdoGetU8 (uint16_t slave, uint16_t index, uint8_t subidx)
{
    lenght = sizeof(usdo) - 1;
    memset(&usdo, 0, 128);
    ec_SDOread(slave, index, subidx, FALSE, &lenght, &usdo, EC_TIMEOUTRXM);
    return usdo[0];
}
int8_t EtherCatApi::sdoGetI8 (uint16_t slave, uint16_t index, uint8_t subidx)
{
    VAR value;
    lenght = sizeof(usdo) - 1;
    memset(&usdo, 0, 128);
    ec_SDOread(slave, index, subidx, FALSE, &lenght, &usdo, EC_TIMEOUTRXM);
    value.uint8Value[0] = usdo[0];
    return value.int8Value[0];
}
uint16_t EtherCatApi::sdoGetU16 (uint16_t slave, uint16_t index, uint8_t subidx)
{
    VAR value;
    lenght = sizeof(usdo) - 1;
    memset(&usdo, 0, 128);
    ec_SDOread(slave, index, subidx, FALSE, &lenght, &usdo, EC_TIMEOUTRXM);
    value.uint8Value[0] = usdo[0];
    value.uint8Value[1] = usdo[1];
    return value.uint16Value[0];
}
int16_t EtherCatApi::sdoGetI16 (uint16_t slave, uint16_t index, uint8_t subidx)
{
    VAR value;
    lenght = sizeof(usdo) - 1;
    memset(&usdo, 0, 128);
    ec_SDOread(slave, index, subidx, FALSE, &lenght, &usdo, EC_TIMEOUTRXM);
    value.uint8Value[0] = usdo[0];
    value.uint8Value[1] = usdo[1];
    return value.int16Value[0];
}
uint32_t EtherCatApi::sdoGetU32(uint16_t slave, uint16_t index, uint8_t subidx)
{
    VAR value;
    lenght = sizeof(usdo) - 1;
    memset(&usdo, 0, 128);
    ec_SDOread(slave, index, subidx, FALSE, &lenght, &usdo, EC_TIMEOUTRXM);
    value.uint8Value[0] = usdo[0];
    value.uint8Value[1] = usdo[1];
    value.uint8Value[2] = usdo[2];
    value.uint8Value[3] = usdo[3];
    return value.uint32Value;
}
int32_t EtherCatApi::sdoGetI32 (uint16_t slave, uint16_t index, uint8_t subidx)
{
    VAR value;
    lenght = sizeof(usdo) - 1;
    memset(&usdo, 0, 128);
    ec_SDOread(slave, index, subidx, FALSE, &lenght, &usdo, EC_TIMEOUTRXM);
    value.uint8Value[0] = usdo[0];
    value.uint8Value[1] = usdo[1];
    value.uint8Value[2] = usdo[2];
    value.uint8Value[3] = usdo[3];
    return value.int32Value;
}
void EtherCatApi::sdoSetBit (uint16_t slave, uint16_t index, uint8_t subidx, bool value )
{
    static bool staticValue;
    staticValue =value;
    ec_SDOwrite( slave,index,subidx,FALSE,sizeof(staticValue),&staticValue,EC_TIMEOUTRXM);
}
void EtherCatApi::sdoSetU8 (uint16_t slave, uint16_t index, uint8_t subidx, uint8_t value )
{
    static uint8_t staticValue;
    staticValue =value;
    ec_SDOwrite( slave,index,subidx,FALSE,sizeof(staticValue),&staticValue,EC_TIMEOUTRXM);
}
void EtherCatApi::sdoSetI8 (uint16_t slave, uint16_t index, uint8_t subidx, int8_t value )
{
    static int8_t staticValue;
    staticValue =value;
    ec_SDOwrite( slave,index,subidx,FALSE,sizeof(staticValue),&staticValue,EC_TIMEOUTRXM);
}
void EtherCatApi::sdoSetU16 (uint16_t slave, uint16_t index, uint8_t subidx, uint16_t value )
{
    static uint16_t staticValue;
    staticValue =value;
    ec_SDOwrite( slave,index,subidx,FALSE,sizeof(staticValue),&staticValue,EC_TIMEOUTRXM);
}
void EtherCatApi::sdoSetI16 (uint16_t slave, uint16_t index, uint8_t subidx, int16_t value )
{
    static int16_t staticValue;
    staticValue =value;
    ec_SDOwrite( slave,index,subidx,FALSE,sizeof(staticValue),&staticValue,EC_TIMEOUTRXM);
}
void EtherCatApi::sdoSetU32(uint16_t slave, uint16_t index, uint8_t subidx, uint32_t value )
{
    static uint32_t staticValue;
    staticValue =value;
    ec_SDOwrite( slave,index,subidx,FALSE,sizeof(staticValue),&staticValue,EC_TIMEOUTRXM);
}
void EtherCatApi::sdoSetI32 (uint16_t slave, uint16_t index, uint8_t subidx, int32_t value )
{
    static int32_t staticValue;
    staticValue =value;
    ec_SDOwrite( slave,index,subidx,FALSE,sizeof(staticValue),&staticValue,EC_TIMEOUTRXM);
}
bool EtherCatApi::pdoGetBit (uint16_t slave, uint16_t index, bool inputs )
{
    bool value;
    if(inputs)
    {
        value =*(ec_slave[slave].inputs + index/10) & static_cast<uint8>(0x01 << index % 10);
    }
    else
    {
        value =*(ec_slave[slave].outputs + index/10) & static_cast<uint8>(0x01 << index % 10);
    }
    return value;
}
uint8_t EtherCatApi::pdoGetU8 (uint16_t slave, uint16_t index, bool inputs )
{
    uint8_t value;
    if(inputs)
    {
        value =*(ec_slave[slave].inputs + index);
    }
    else
    {
        value =*(ec_slave[slave].outputs + index);
    }
    return value;
}

int8_t EtherCatApi::pdoGetI8 (uint16_t slave, uint16_t index, bool inputs )
{
    VAR value;
    if(inputs)
    {
        value.uint8Value[0] =*(ec_slave[slave].inputs + index);
    }
    else
    {
        value.uint8Value[0] =*(ec_slave[slave].outputs + index);
    }
    return value.int8Value[0];
}
uint16_t EtherCatApi::pdoGetU16 (uint16_t slave, uint16_t index, bool inputs )
{
    VAR value;
    if(inputs)
    {
        value.uint8Value[0] =*(ec_slave[slave].inputs + index);
        value.uint8Value[1] =*(ec_slave[slave].inputs + index + 1);
    }
    else
    {
        value.uint8Value[0] =*(ec_slave[slave].outputs + index);
        value.uint8Value[1] =*(ec_slave[slave].outputs + index + 1);
    }
    return value.uint16Value[0];
}
int16_t EtherCatApi::pdoGetI16 (uint16_t slave, uint16_t index, bool inputs )
{
    VAR value;
    if(inputs)
    {
        value.uint8Value[0] =*(ec_slave[slave].inputs + index);
        value.uint8Value[1] =*(ec_slave[slave].inputs + index + 1);
    }
    else
    {
        value.uint8Value[0] =*(ec_slave[slave].outputs + index);
        value.uint8Value[1] =*(ec_slave[slave].outputs + index + 1);
    }
    return value.int16Value[0];
}
uint32_t EtherCatApi::pdoGetU32(uint16_t slave, uint16_t index, bool inputs )
{
    VAR value;
    if(inputs)
    {
        value.uint8Value[0] =*(ec_slave[slave].inputs + index);
        value.uint8Value[1] =*(ec_slave[slave].inputs + index + 1);
        value.uint8Value[2] =*(ec_slave[slave].inputs + index + 2);
        value.uint8Value[3] =*(ec_slave[slave].inputs + index + 3);
    }
    else
    {
        value.uint8Value[0] =*(ec_slave[slave].outputs + index);
        value.uint8Value[1] =*(ec_slave[slave].outputs + index + 1);
        value.uint8Value[2] =*(ec_slave[slave].outputs + index + 2);
        value.uint8Value[3] =*(ec_slave[slave].outputs + index + 3);
    }
    return value.uint32Value;
}
int32_t EtherCatApi::pdoGetI32 (uint16_t slave, uint16_t index, bool inputs )
{
    VAR value;
    if(inputs)
    {
        value.uint8Value[0] =*(ec_slave[slave].inputs + index);
        value.uint8Value[1] =*(ec_slave[slave].inputs + index + 1);
        value.uint8Value[2] =*(ec_slave[slave].inputs + index + 2);
        value.uint8Value[3] =*(ec_slave[slave].inputs + index + 3);
    }
    else
    {
        value.uint8Value[0] =*(ec_slave[slave].outputs + index);
        value.uint8Value[1] =*(ec_slave[slave].outputs + index + 1);
        value.uint8Value[2] =*(ec_slave[slave].outputs + index + 2);
        value.uint8Value[3] =*(ec_slave[slave].outputs + index + 3);
    }
    return value.int32Value;
}
void EtherCatApi::pdoSetBit (uint16_t slave, uint16_t index, bool value )
{
    if(value)
    {
        *(ec_slave[slave].outputs + index/10) |= (0x01 << index%10);
    }
    else
    {
        *(ec_slave[slave].outputs + index/10) &= ~(0x01 << index%10);
    }
}

void EtherCatApi::pdoSetU8 (uint16_t slave, uint16_t index, uint8_t value )
{
   *(ec_slave[slave].outputs + index) = value;
    return;
}
void EtherCatApi::pdoSetI8 (uint16_t slave, uint16_t index, int8_t value )
{
    VAR varValue;
    varValue.int8Value[0] = value;
    *(ec_slave[slave].outputs + index) = varValue.uint8Value[0];
     return;
}
void EtherCatApi::pdoSetU16 (uint16_t slave, uint16_t index, uint16_t value )
{
    VAR varValue;
    varValue.uint16Value[0] = value;
    *(ec_slave[slave].outputs + index) = varValue.uint8Value[0];
    *(ec_slave[slave].outputs + index + 1) = varValue.uint8Value[1];
     return;
}
void EtherCatApi::pdoSetI16 (uint16_t slave, uint16_t index, int16_t value )
{
    VAR varValue;
    varValue.int16Value[0] = value;
    *(ec_slave[slave].outputs + index) = varValue.uint8Value[0];
    *(ec_slave[slave].outputs + index + 1) = varValue.uint8Value[1];
     return;
}
void EtherCatApi::pdoSetU32(uint16_t slave, uint16_t index, uint32_t value )
{
    VAR varValue;
    varValue.uint32Value = value;
    *(ec_slave[slave].outputs + index) = varValue.uint8Value[0];
    *(ec_slave[slave].outputs + index + 1) = varValue.uint8Value[1];
    *(ec_slave[slave].outputs + index + 2) = varValue.uint8Value[2];
    *(ec_slave[slave].outputs + index + 3) = varValue.uint8Value[3];
     return;
}
void EtherCatApi::pdoSetI32 (uint16_t slave, uint16_t index, int32_t value )
{
    VAR varValue;
    varValue.int32Value = value;
    *(ec_slave[slave].outputs + index) = varValue.uint8Value[0];
    *(ec_slave[slave].outputs + index + 1) = varValue.uint8Value[1];
    *(ec_slave[slave].outputs + index + 2) = varValue.uint8Value[2];
    *(ec_slave[slave].outputs + index + 3) = varValue.uint8Value[3];
     return;
}
