#ifndef ETHERCATAPI_H
#define ETHERCATAPI_H

#include <QWidget>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <sched.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <math.h>
#include <inttypes.h>
#include <time.h>
#include "ethercat.h"
#include "apitypes.h"
#define NSEC_PER_SEC 1000000000
#define EC_TIMEOUTMON 500
#define EK1100_1           1
#define EL7041_1           2
#define NUMBER_OF_SLAVES   2

namespace EtherCatApi
{
    bool sdoGetBit (uint16_t slave, uint16_t index, uint8_t subidx);
    uint8_t sdoGetU8 (uint16_t slave, uint16_t index, uint8_t subidx);
    int8_t sdoGetI8 (uint16_t slave, uint16_t index, uint8_t subidx);
    uint16_t sdoGetU16 (uint16_t slave, uint16_t index, uint8_t subidx);
    int16_t sdoGetI16 (uint16_t slave, uint16_t index, uint8_t subidx);
    uint32_t sdoGetU32(uint16_t slave, uint16_t index, uint8_t subidx);
    int32_t sdoGetI32 (uint16_t slave, uint16_t index, uint8_t subidx);
    void sdoSetBit (uint16_t slave, uint16_t index, uint8_t subidx, bool value );
    void sdoSetU8 (uint16_t slave, uint16_t index, uint8_t subidx, uint8_t value );
    void sdoSetI8 (uint16_t slave, uint16_t index, uint8_t subidx, int8_t value );
    void sdoSetU16 (uint16_t slave, uint16_t index, uint8_t subidx, uint16_t value );
    void sdoSetI16 (uint16_t slave, uint16_t index, uint8_t subidx, int16_t value );
    void sdoSetU32(uint16_t slave, uint16_t index, uint8_t subidx, uint32_t value );
    void sdoSetI32 (uint16_t slave, uint16_t index, uint8_t subidx, int32_t value );
    bool pdoGetBit (uint16_t slave, uint16_t index, bool inputs );
    uint8_t pdoGetU8 (uint16_t slave, uint16_t index, bool inputs );
    int8_t pdoGetI8 (uint16_t slave, uint16_t index, bool inputs );
    uint16_t pdoGetU16 (uint16_t slave, uint16_t index, bool inputs );
    int16_t pdoGetI16 (uint16_t slave, uint16_t index, bool inputs );
    uint32_t pdoGetU32(uint16_t slave, uint16_t index, bool inputs );
    int32_t pdoGetI32 (uint16_t slave, uint16_t index, bool inputs );
    void pdoSetBit (uint16_t slave, uint16_t index, bool value );
    void pdoSetU8 (uint16_t slave, uint16_t index, uint8_t value );
    void pdoSetI8 (uint16_t slave, uint16_t index, int8_t value );
    void pdoSetU16 (uint16_t slave, uint16_t index, uint16_t value );
    void pdoSetI16 (uint16_t slave, uint16_t index, int16_t value );
    void pdoSetU32(uint16_t slave, uint16_t index, uint32_t value );
    void pdoSetI32 (uint16_t slave, uint16_t index, int32_t value );
    void startEtherCat(void * interface, void * IOmapPtr);
    void stopEtherCat(void);
    bool getInOp();
}
#endif
