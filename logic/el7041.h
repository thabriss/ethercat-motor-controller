#ifndef EL7041_H
#define EL7041_H
//SDO
#define ID_DEVICE_TYPE              0x1000
#define SUB_DEVICE_TYPE             0x00        //0x20 bit
#define ID_DEVICE_NAME              0x1008
#define SUB_DEVICE_NAME             0x00        //0x30 bit
#define ID_HARDWER_VERSION          0x1009
#define SUB_HARDWER_VERSION         0x00        //0x10 bit
#define ID_SOFTWER_VERSION          0x100A
#define SUB_SOFTWER_VERSION         0x00
#define ID_RESTORE_PARAMETERS       0x1011
#define SUB0                        0x00        //0x08 bit
#define SUB1                        0x01        //0x20 bit
#define ID_IDENTITY                    0x1018
#define SUB0                        0x00        //0x08 bit
#define SUB_VENDOR_ID               0x01        //0x20 bit
#define SUB_PRODUCT_CODE            0x02        //0x20 bit
#define SUB_REVISION                0x03        //0x20 bit
#define SUB_SERIAL_NUMBER           0x04        //0x20 bit
#define ID_MOTOR_SETINGS            0x8010
#define SUB_MAX_CURRENT             0x01
#define SUB_REDUCED_CURRENT         0x02
#define SUB_NOMINAL_VOLTAGE         0x03
#define SUB_COIL_RESISTANCE         0x04
#define SUB_EMF                     0x05
#define SUB_MOTOR_FULLSTEPS         0x06

//PDO
//definicja wyjsc
#define OUT_ENABLE_LATCH_C 			00
#define OUT_POSITIVE_EDGE 			01
#define OUT_SET_COUNTER 			02
#define OUT_NEGATIVE_EDGE 			03
#define OUT_SET_COUNTER_VALUE 		2
#define OUT_ENABLE                  40
#define OUT_RESET                   41
#define OUT_REDUCE_TORQUE			42
#define OUT_VELOCITY				6


//definicja wejsc
#define IN_LATCH_C_VALID			0
#define IN_LATCH_EXTERN_VALID		1
#define IN_SET_COUNTER_DONE			2
#define IN_COUNTER_UNDERFLOW		3
#define IN_COUNTER_OVERFLOW			4
#define IN_EXTRAPOLATION_STALL		7
#define IN_STATUS_OF_INPUT_A		10
#define IN_STATUS_OF_INPUT_B		11
#define IN_STATUS_OF_INPUT_C		12
#define IN_STATUS_OF_EXTERN_LATCH	14
#define IN_SYNC_ERROR				15
#define IN_TXPDO_TOGGLE1			17
#define IN_COUNTER_VALUE			2
#define IN_LATCH_VALUE				4
#define IN_READY_TO_ENABLE			60
#define IN_READ						61
#define IN_WARNING					62
#define IN_ERROR					63
#define IN_MOVING_POSITIVE			64
#define IN_MOVING_NEGATIVE			65
#define IN_TORQUE_REDUCED			66
#define IN_DIGITAL_INPUT1			73
#define IN_DIGITAL_INPUT2			74
#define IN_SYNC_ERROR2				75
#define IN_TXPDO_TOGGLE2			77

#endif // EL7041_H
