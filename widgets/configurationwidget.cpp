#include "configurationwidget.h"
#include "ui_configurationwidget.h"
#include "el7041.h"

using namespace EtherCatApi;
ConfigurationWidget::ConfigurationWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConfigurationWidget)
{
    ui->setupUi(this);

    connect(ui->sendPushButton, &QPushButton::clicked,
            this, &ConfigurationWidget::sendConfiguration);
    connect(ui->readPushButton, &QPushButton::clicked,
            this, &ConfigurationWidget::readConfiguration);
}

ConfigurationWidget::~ConfigurationWidget()
{
    delete ui;
}

void ConfigurationWidget::readConfiguration()
{
    if(EtherCatApi::getInOp())
    {
        ui->maxCurrentDoubleSpinBox->setValue(EtherCatApi::sdoGetU16(EL7041_1,ID_MOTOR_SETINGS, SUB_MAX_CURRENT)/1000.0);
        ui->reducedCurrentDoubleSpinBox->setValue(EtherCatApi::sdoGetU16(EL7041_1, ID_MOTOR_SETINGS, SUB_REDUCED_CURRENT)/1000.0);
        ui->nominalVoltageDoubleSpinBox->setValue(EtherCatApi::sdoGetU16(EL7041_1, ID_MOTOR_SETINGS, SUB_NOMINAL_VOLTAGE)/1000.0);
        ui->coilResistanceDoubleSpinBox->setValue(EtherCatApi::sdoGetU16(EL7041_1, ID_MOTOR_SETINGS, SUB_COIL_RESISTANCE)/100.0);
        ui->emfDoubleSpinBox->setValue(EtherCatApi::sdoGetU16(EL7041_1, ID_MOTOR_SETINGS, SUB_EMF));
        ui->fullstepsDoubleSpinBox->setValue(EtherCatApi::sdoGetU16(EL7041_1, ID_MOTOR_SETINGS, SUB_MOTOR_FULLSTEPS));
    }
}

void ConfigurationWidget::sendConfiguration()
{
    if(EtherCatApi::getInOp())
    {
        EtherCatApi::sdoSetU16(EL7041_1,ID_MOTOR_SETINGS, SUB_MAX_CURRENT, static_cast<uint16_t>(ui->maxCurrentDoubleSpinBox->value()*1000.0));
        EtherCatApi::sdoSetU16(EL7041_1,ID_MOTOR_SETINGS, SUB_NOMINAL_VOLTAGE, static_cast<uint16_t>(ui->nominalVoltageDoubleSpinBox->value()*1000.0));
        EtherCatApi::sdoSetU16(EL7041_1,ID_MOTOR_SETINGS, SUB_REDUCED_CURRENT, static_cast<uint16_t>(ui->reducedCurrentDoubleSpinBox->value()*1000.0));
        EtherCatApi::sdoSetU16(EL7041_1,ID_MOTOR_SETINGS, SUB_COIL_RESISTANCE, static_cast<uint16_t>(ui->coilResistanceDoubleSpinBox->value()*100.0));
        EtherCatApi::sdoSetU16(EL7041_1,ID_MOTOR_SETINGS, SUB_EMF, static_cast<uint16_t>(ui->emfDoubleSpinBox->value()));
        EtherCatApi::sdoSetU16(EL7041_1,ID_MOTOR_SETINGS, SUB_MOTOR_FULLSTEPS, static_cast<uint16_t>(ui->fullstepsDoubleSpinBox->value()));
    }
}
