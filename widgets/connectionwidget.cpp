#include "connectionwidget.h"
#include "ui_connectionwidget.h"
#include "mainwindow.h"
#include "ethercatapi.h"
#include "el7041.h"

using namespace EtherCatApi;

ConnectionWidget::ConnectionWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConnectionWidget)
{
    ui->setupUi(this);
    refreshInterfacesList();

    ioMapPtr = malloc(4096u);
    memset(ioMapPtr, 0, 4096);
    interfacePtr = malloc(10u);

    isConnected = false;
    opStateTim->setInterval(100);

    connect(ui->refreshPushButton, &QPushButton::clicked,
            this, &ConnectionWidget::refreshInterfacesList);

    connect(ui->connectPushButton, &QPushButton::clicked,
            this, &ConnectionWidget::toggleComm);
}

ConnectionWidget::~ConnectionWidget()
{
    free(ioMapPtr);
    free(interfacePtr);
    delete ui;
}

void ConnectionWidget::refreshInterfacesList()
{
    ui->interfaceComboBox->clear();
    interfaces = QNetworkInterface::allInterfaces();
    for (int i =0;i<interfaces.size();i++)
    {
        ui->interfaceComboBox->addItem(interfaces[i].name());
    }
}

void ConnectionWidget::toggleComm()
{
    if(!isConnected)
    {
        isConnected = true;
        QString qstringInterface = ui->interfaceComboBox->currentText();
        const char * charsInterface = qstringInterface.toStdString().c_str();
        interfacePtr = (void *) charsInterface;
        EtherCatApi::startEtherCat(interfacePtr, ioMapPtr);
        ui->connectPushButton->setText("Rozłącz");
    }
    else
    {
        isConnected = false;
        EtherCatApi::stopEtherCat();
        ui->connectPushButton->setText("Połącz");
    }
}
