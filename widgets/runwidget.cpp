#include "runwidget.h"
#include "ui_runwidget.h"
#include "ethercatapi.h"
#include "el7041.h"

using namespace EtherCatApi;

RunWidget::RunWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RunWidget)
{
    ui->setupUi(this);

    connect(ui->StartPushButton, &QPushButton::clicked,
            this, &RunWidget::enableMotor);
    connect(ui->stopPushButton, &QPushButton::clicked,
            this, &RunWidget::disableMotor);
}

RunWidget::~RunWidget()
{
    delete ui;
}

void RunWidget::enableMotor()
{
    if(EtherCatApi::getInOp())
    {
        EtherCatApi::pdoSetBit(EL7041_1, OUT_ENABLE, true);
        EtherCatApi::pdoSetU16(EL7041_1, OUT_SET_COUNTER_VALUE, 0xFFFF);
        EtherCatApi::pdoSetI16(EL7041_1, OUT_VELOCITY, (int16_t)ui->refVelocityDoubleSpinBox->value());
    }
}

void RunWidget::disableMotor()
{
    if(EtherCatApi::getInOp())
    {
        EtherCatApi::pdoSetBit(EL7041_1, OUT_ENABLE, false);
    }
}
