#ifndef POSITIONCONTROLLERWIDGET_H
#define POSITIONCONTROLLERWIDGET_H

#include <QWidget>
#include <QFile>
#include <QTextStream>
#include <QTimer>
#include "ethercatapi.h"
#include "el7041.h"

namespace Ui {
class PositionControllerWidget;
}

class PositionControllerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PositionControllerWidget(QWidget *parent = nullptr);
    ~PositionControllerWidget();

private:
    void startPositioning(void);
    void stopPositioning(void);
    void setVelocity(void);
    Ui::PositionControllerWidget *ui;
    QFile * pFile;
    QTextStream * pLog;
    QTimer * timer;


};

#endif // POSITIONCONTROLLERWIDGET_H
