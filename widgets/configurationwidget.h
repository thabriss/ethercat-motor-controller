#ifndef CONFIGURATIONWIDGET_H
#define CONFIGURATIONWIDGET_H

#include <QWidget>
#include "ethercatapi.h"

namespace Ui {
class ConfigurationWidget;
}

class ConfigurationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConfigurationWidget(QWidget *parent = 0);
    ~ConfigurationWidget();

private:
    Ui::ConfigurationWidget *ui;

    void readConfiguration();
    void sendConfiguration();
};

#endif // CONFIGURATIONWIDGET_H
