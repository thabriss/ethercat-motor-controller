#include "positioncontrollerwidget.h"
#include "ui_positioncontrollerwidget.h"


PositionControllerWidget::PositionControllerWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PositionControllerWidget)
{
    ui->setupUi(this);
    static QFile file("log.txt");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    static QTextStream log(&file);
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout,
            this, &PositionControllerWidget::setVelocity);
    connect(ui->startPushButton, &QPushButton::clicked,
            this, &PositionControllerWidget::startPositioning);
    connect(ui->stopPushButton, &QPushButton::clicked,
            this, &PositionControllerWidget::stopPositioning);
}

PositionControllerWidget::~PositionControllerWidget()
{
    delete ui;
}

void PositionControllerWidget::startPositioning(void)
{
    if(EtherCatApi::getInOp())
    {
        if(ui->stepsSpinBox->value()>0)
        {
            EtherCatApi::pdoSetU16(EL7041_1,OUT_SET_COUNTER_VALUE, 0);
        }
        else
        {
            EtherCatApi::pdoSetU16(EL7041_1,OUT_SET_COUNTER_VALUE, 0xFFFFu);
        }
        EtherCatApi::pdoSetBit(EL7041_1,OUT_SET_COUNTER, true);
        ui->kpDoubleSpinBox -> setEnabled(false);
        ui->stepsSpinBox ->setEnabled(false);
        ui->startPushButton->setEnabled(false);

        timer->start(1);
    }
}

void PositionControllerWidget::stopPositioning(void)
{
    if(EtherCatApi::getInOp())
    {
        timer->stop();
        EtherCatApi::pdoSetBit(EL7041_1, OUT_ENABLE, false);
        ui->kpDoubleSpinBox -> setEnabled(true);
        ui->stepsSpinBox ->setEnabled(true);
        ui->startPushButton->setEnabled(true);
    }
}

void PositionControllerWidget::setVelocity(void)
{
    EtherCatApi::pdoSetBit(EL7041_1,OUT_SET_COUNTER, false);
    int32_t setValue= static_cast<int32_t>(ui->stepsSpinBox->value());
    int32_t actualValue = static_cast<int32_t>(EtherCatApi::pdoGetU16(EL7041_1, IN_COUNTER_VALUE, true));
    int32_t error;
    int64_t velocity;
    if(setValue > 0)
    {
        error  = static_cast<int32_t>(setValue - actualValue);
        velocity = error * ui->kpDoubleSpinBox->value();
        if(velocity > ui->maxVelocitySpinBox->value())
        {
            velocity =  ui->maxVelocitySpinBox->value();
        }
    }
    else
    {
        error = static_cast<int32_t>(0xFFFFu + setValue - actualValue);
        velocity = error * ui->kpDoubleSpinBox->value();
        if(velocity < (-1 * ui->maxVelocitySpinBox->value()))
        {
            velocity =  -1 * ui->maxVelocitySpinBox->value();
        }
    }
    if(error ==0)
    {
        ui->kpDoubleSpinBox -> setEnabled(true);
        ui->stepsSpinBox ->setEnabled(true);
        ui->startPushButton->setEnabled(true);
        EtherCatApi::pdoSetBit(EL7041_1, OUT_ENABLE, false);
        EtherCatApi::pdoSetI16(EL7041_1, OUT_VELOCITY, 0);
        timer->stop();

    }
    else
    {
        EtherCatApi::pdoSetBit(EL7041_1, OUT_ENABLE, true);
        EtherCatApi::pdoSetI16(EL7041_1, OUT_VELOCITY, static_cast<int16_t>(velocity));
    }
}
