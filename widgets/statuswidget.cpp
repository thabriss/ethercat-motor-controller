#include "statuswidget.h"
#include "ui_statuswidget.h"
#include "el7041.h"
#include "ethercatapi.h"

using namespace EtherCatApi;

StatusWidget::StatusWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StatusWidget)
{
    ui->setupUi(this);

    readValuesTimer->setInterval(100);
    readValuesTimer->start();

    connect(readValuesTimer, &QTimer::timeout,
            this, &StatusWidget::getNewPdo);
}

StatusWidget::~StatusWidget()
{
    delete ui;
}

void StatusWidget::getNewPdo()
{
    if(EtherCatApi::getInOp() != 0)
    {
        ui->status1DoubleSpinBox->setValue((double)EtherCatApi::pdoGetU8(EL7041_1, 0, true));
        ui->status2DoubleSpinBox->setValue((double)EtherCatApi::pdoGetU8(EL7041_1, 1, true));
        ui->counterDoubleSpinBox->setValue((double)EtherCatApi::pdoGetU16(EL7041_1, IN_COUNTER_VALUE, true));
        ui->latchDoubleSpinBox->setValue((double)EtherCatApi::pdoGetU16(EL7041_1, IN_LATCH_VALUE, true));
        ui->status3DoubleSpinBox->setValue((double)EtherCatApi::pdoGetU8(EL7041_1, 6, true));
        ui->status4DoubleSpinBox->setValue((double)EtherCatApi::pdoGetU8(EL7041_1, 7, true));
    }
}
