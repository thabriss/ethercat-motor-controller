#ifndef STATUSWIDGET_H
#define STATUSWIDGET_H

#include <QWidget>
#include <QTimer>

namespace Ui {
class StatusWidget;
}

class StatusWidget : public QWidget
{
    Q_OBJECT

public:
    explicit StatusWidget(QWidget *parent = nullptr);
    ~StatusWidget();

private slots:
    void getNewPdo();

private:
    Ui::StatusWidget *ui;
    QTimer* readValuesTimer = new QTimer(this);
};

#endif // STATUSWIDGET_H
