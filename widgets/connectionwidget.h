#ifndef CONNECTIONWIDGET_H
#define CONNECTIONWIDGET_H

#include <QWidget>
#include <QNetworkInterface>
#include <QTimer>

namespace Ui {
class ConnectionWidget;
}

class ConnectionWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConnectionWidget(QWidget *parent = 0);
    ~ConnectionWidget();

private slots:
    void refreshInterfacesList();
    void toggleComm();

private:
    Ui::ConnectionWidget *ui;
    QList<QNetworkInterface> interfaces;

    void* ioMapPtr;
    void* interfacePtr;

    bool isConnected;
    QTimer* opStateTim = new QTimer(this);
};

#endif // CONNECTIONWIDGET_H
