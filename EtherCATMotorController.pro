#-------------------------------------------------
#
# Project created by QtCreator 2019-01-19T11:06:33
#
#-------------------------------------------------

QT       += core gui
QMAKE_CXXFLAGS += -std=c++0x
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QT += network

TARGET = EtherCATMotorController
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
INCLUDEPATH +=  widgets\
                logic\



# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
    logic/ethercatapi.cpp \
        mainwindow.cpp \
    widgets/positioncontrollerwidget.cpp \
    widgets/connectionwidget.cpp \
    widgets/configurationwidget.cpp \
    widgets/controllerwidget.cpp \
    widgets/runwidget.cpp \
    logic/ethercatbase.c \
    logic/ethercatcoe.c \
    logic/ethercatconfig.c \
    logic/ethercatdc.c \
    logic/ethercatfoe.c \
    logic/ethercatmain.c \
    logic/ethercatprint.c \
    logic/ethercatsoe.c \
    logic/nicdrv.c \
    logic/osal.c \
    logic/oshw.c \
    widgets/statuswidget.cpp

HEADERS  += mainwindow.h \
    logic/apitypes.h \
    logic/el7041.h \
    logic/ethercatapi.h \
    widgets/positioncontrollerwidget.h \
    widgets/connectionwidget.h \
    widgets/configurationwidget.h \
    widgets/controllerwidget.h \
    widgets/runwidget.h \
    logic/ethercat.h \
    logic/ethercatbase.h \
    logic/ethercatcoe.h \
    logic/ethercatconfig.h \
    logic/ethercatconfiglist.h \
    logic/ethercatdc.h \
    logic/ethercatfoe.h \
    logic/ethercatmain.h \
    logic/ethercatprint.h \
    logic/ethercatsoe.h \
    logic/ethercattype.h \
    logic/nicdrv.h \
    logic/osal.h \
    logic/osal_defs.h \
    logic/oshw.h \
    widgets/statuswidget.h

FORMS    += mainwindow.ui \
    widgets/positioncontrollerwidget.ui \
    widgets/connectionwidget.ui \
    widgets/configurationwidget.ui \
    widgets/controllerwidget.ui \
    widgets/runwidget.ui \
    widgets/statuswidget.ui
